import { User } from "./user";

export class Device
{
    id: number;
    serial: string;
    brand : string;
    model : string;
    user: User;

    constructor(id?:number, serial?:string, model?:string,user?:User)
    {

    }
}
