export class User {
    id: number;
    firstname: string;
    lastname: string;
    phone: string;
    email: string;
    password: string;
    link: string;

    constructor (id?:number, firstname?:string, lastname?:string, phone?:string, email?:string, password?:string, link?:string){
        this.id = id;
        this.firstname = firstname;
        this.lastname = lastname;
        this.phone = phone;
        this.email = email;
        this.password = password;
        this.link = link;
    }
}

