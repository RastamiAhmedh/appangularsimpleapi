import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes,ActivatedRoute,ParamMap } from '@angular/router';
import { NewUserComponent } from './Component/user/newUser/NewUser.component';
import {ListUserComponent} from './Component/user/listUser/listUser.component';

const routes : Routes= [
  { path : 'users/new', component: NewUserComponent},
  { path : 'users', component: ListUserComponent},  
  { path :'', redirectTo :'users/new', pathMatch : 'full'},
];



@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forRoot(routes)
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
