
import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/Model/user';
import { HttpClient, HttpErrorResponse, HttpHeaders, HttpResponse } from '@angular/common/http'

@Component({
  selector: 'app-user',
  templateUrl: './ListUser.component.html',
  styleUrls: ['./ListUser.component.css']
})
export class ListUserComponent implements OnInit {
  APIUrl = 'http://localhost/simple-api/users/';
  user: User;
  users : User[];
  submitted = false;
  userForm: User;
  errors;
  constructor(private http:HttpClient) { }

  ngOnInit(): void {
    this.user = new User();
    this.userForm = new User();
    this.user.email = "user@user.fr";
    this.user.firstname = "Utilisateur";
    this.user.lastname = "";
    this.readAPI(this.APIUrl)
        .subscribe((dataReceived) => {
        var data = dataReceived['data'];
        this.users = new Array;
        data.forEach(data => {
            console.log(data);
            let link = data.link;
            var thisUser = new User(null,data.firstname,data.lastname,null,data.email,null);
            this.users.push(thisUser);
        });
      });
    


  }

  readAPI(URL: string){

    return this.http.get(URL,
      { headers: new HttpHeaders({'Content-Type': 'application/json','SimpleApiKey': 'myOwnSecretApiKey'})}
    );
  }
}
